# QtDAQ #

QtDAQ is an open-source digital data aquisition (DAQ) software package, written in C++, utilising the Qt5 framework. 

QtDAQ is designed to handle the readout and analysis of digitised signals from several digitizers, as well as temperature monitoring and detector voltage control through communication with a microcontroller. Digitised signals can be saved to disk in unprocessed form and read back in QtDAQ for offline analysis.

### Features ###

* Support for a number of commercial digitizers
* Multi-threaded acquisition and analysis 
* Embedded Google V8 JavaScript engine for custom analysis routines
* Compressed output using LZO or ZLib